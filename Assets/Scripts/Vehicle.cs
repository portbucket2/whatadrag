using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : MonoBehaviour
{
    public ItemType itemType;

    public ItemSpawner itemSpawner;

    public GameObject ropeObject;

    public Transform targetTrans;

    public int vehicleCoinValue;

    public bool isDividerCalled;

    public bool isAlreadyCollided;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DIVIDER") && !isDividerCalled)
        {
            isDividerCalled = true;
            Debug.Log("Collided " + other.name);

            targetTrans = other.GetComponent<Divider>().warehouseTrans;


            itemSpawner.isOccupied = false;
            PlayerController.Instance.DecreaseRopeCounter();
            PlayerController.Instance.DecreaseWeight(GetComponent<Rigidbody>().mass);
            Gameplay.Instance.InstantiateItem(itemType);



            StartCoroutine(MovetoWarehouseRoutine());
            StartCoroutine(DestroyRope(ropeObject));
        }

        if (other.CompareTag("WAREHOUSE"))
        {
            StartCoroutine(ScaleDownRoutine());
        }
    }

    IEnumerator DestroyRope(GameObject go)
    {
        yield return new WaitForEndOfFrame();
        DestroyImmediate(go);
    }

    IEnumerator ScaleDownRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 0.5f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = transform.localScale;
        Vector3 t_DestValue = Vector3.zero;
      
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.localScale = Vector3.Slerp(t_CurrentFillValue, t_DestValue, t_Progression);

            if (t_Progression >= 1f)
            {
                Destroy(transform.root.gameObject, 0.1f);
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(ScaleDownRoutine());
    }

    private IEnumerator MovetoWarehouseRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = .75f;
        float t_EndTIme = Time.time + t_Duration;
        Vector3 t_CurrentFillValue = transform.position;

        Vector3 t_DestValue = (targetTrans.position - t_CurrentFillValue);
       
        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            transform.position = Vector3.Slerp(t_CurrentFillValue, targetTrans.position, t_Progression);

            if (t_Progression >= 1f)
            {
               
                break;
            }

            yield return t_CycleDelay;
        }

        Cashup.Instance.ActivateCashUp(vehicleCoinValue);
        yield return new WaitForSeconds(0.3f);

        Currency.Transaction(CurrencyType.CASH, vehicleCoinValue);
        yield return new WaitForSeconds(0.2f);

        StopCoroutine(MovetoWarehouseRoutine());
    }

    public void SetSpawner(ItemSpawner itemSpawner)
    {
        this.itemSpawner = itemSpawner;
    }
}
