﻿
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public DynamicJoystick dynamicJoystick;

    public GameObject playerObject;
    public Transform playerTrans;


    [Range(0f, 100f)] public float movementSpeed;
    [Range(0f, 100f)] public float playerMovement;


    public bool isTakingInput;

    [Header("Animation")]
    public Animator animator;

    private int RUN = Animator.StringToHash("run");
    private int IDLE = Animator.StringToHash("idle");
    private int DRAG = Animator.StringToHash("drag");

    private int DUMBLE = Animator.StringToHash("dumble");

    

    private int SIT_UP = Animator.StringToHash("situp");
    private int TRADE_MILL = Animator.StringToHash("trademil");


    public bool isRopeAttached;

    public bool isInGym;
    public bool isgymAnim;

    public Transform gymPos;

    public GameObject dumbBell;
    public GameObject barBell1;
    public GameObject barbell2;
    public GameObject tradMill;

    [Space(20)]
    public int attachedRopeCounter;


    public CameraFollow camFollow;

    public float currentCarryingWeight;



    public static PlayerController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

   

    private void Update()
    {
       
    }

    private void FixedUpdate()
    {
        if (!isInGym)
        {
            Vector3 direction = Vector3.forward * dynamicJoystick.Vertical + Vector3.right * dynamicJoystick.Horizontal;

            Vector3 t_ModifiedDirection = direction * movementSpeed * Time.fixedDeltaTime;

            if (t_ModifiedDirection.magnitude > 0)
            {
                playerObject.transform.eulerAngles = new Vector3(0, Mathf.Atan2(dynamicJoystick.Horizontal / 3f, dynamicJoystick.Vertical / 3f) * 180 / Mathf.PI, 0);
            }
            else
            {

            }

            if (direction.x > 0 || direction.z > 0 || direction.x < 0 || direction.z < 0)
            {
                if (isRopeAttached)
                {
                    PlayDrag();
                }
                else
                {
                    PlayRun();
                }
            }
            else
            {
                PlayIdle();
            }

            if (isTakingInput)
            {
                playerObject.GetComponent<Rigidbody>().velocity = t_ModifiedDirection * playerMovement;
            }
        }
        else
        {
            if (!isgymAnim)
            {
                PlayIdle();
            }
        }
    }

   
    public void ActivateDynamicJoystick()
    {
        dynamicJoystick.Reset();
        
        isTakingInput = true;
        dynamicJoystick.gameObject.SetActive(true);
    }

    public void DeactivateDynamicJoystick()
    {
        dynamicJoystick.Reset();
        playerObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        isTakingInput = false;
        dynamicJoystick.gameObject.SetActive(false);
    }

    public void SetPlayerPos(Vector3 t_Pos)
    {
        playerObject.transform.position = t_Pos;
    }

    public Transform GetPlayerTrans()
    {
        return playerObject.transform;
    }

    public void SetPlayerLocalPos(Vector3 zero,Transform t_HandTrans)
    {
        StartCoroutine(PlayerMoveToHandRoutine(zero,t_HandTrans));
    }

    IEnumerator PlayerMoveToHandRoutine(Vector3 t_Pos,Transform t_HandTrans)
    {
        while (true)
        {
            playerObject.transform.localPosition = Vector3.Lerp(playerObject.transform.localPosition, t_Pos, 4*Time.deltaTime);

            if (Vector3.Distance(playerObject.transform.localPosition,t_HandTrans.position) < .01f)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.LogWarning(collision.transform.name);
        if (collision.transform.CompareTag("BIKE"))
        {
            if (!collision.transform.GetComponent<Vehicle>().isAlreadyCollided)
            {
                float cycleWeight = collision.transform.GetComponent<Rigidbody>().mass;
                currentCarryingWeight = cycleWeight + currentCarryingWeight;
                if (currentCarryingWeight <= StrengthManager.Instance.currentWorldInfo.maxWeightCarry)
                {
                    if (IsConnectRope())
                    {
                        Debug.Log("BIKE COLLISION");
                        GrapplingHook hook = collision.transform.GetChild(0).GetComponent<GrapplingHook>();
                        hook.ThrowHook();
                        attachedRopeCounter++;
                        isRopeAttached = true;
                        collision.transform.GetComponent<Rigidbody>().isKinematic = false;
                        collision.transform.GetComponent<Vehicle>().isAlreadyCollided = true;
                    }
                }
                else
                {
                    currentCarryingWeight -= cycleWeight;
                    Debug.LogWarning("WEIGHT EXCREEEEEDDD!! " + currentCarryingWeight);
                }
            }
        }
        if (collision.transform.CompareTag("CAR"))
        {
            if (!collision.transform.GetComponent<Vehicle>().isAlreadyCollided)
            {
                float cycleWeight = collision.transform.GetComponent<Rigidbody>().mass;
                currentCarryingWeight = cycleWeight + currentCarryingWeight;
                if (currentCarryingWeight <= StrengthManager.Instance.currentWorldInfo.maxWeightCarry)
                {
                    if (IsConnectRope())
                    {
                        Debug.Log("BIKE COLLISION");
                        GrapplingHook hook = collision.transform.GetChild(0).GetComponent<GrapplingHook>();
                        hook.ThrowHook();
                        attachedRopeCounter++;
                        isRopeAttached = true;
                        collision.transform.GetComponent<Rigidbody>().isKinematic = false;
                        collision.transform.GetComponent<Vehicle>().isAlreadyCollided = true;
                    }
                }
                else
                {
                    currentCarryingWeight -= cycleWeight;
                    Debug.LogWarning("WEIGHT EXCREEEEEDDD!! " + currentCarryingWeight);
                }
            }
        }
        if (collision.transform.CompareTag("CYCLE"))
        {
            if (!collision.transform.GetComponent<Vehicle>().isAlreadyCollided)
            {
                float cycleWeight = collision.transform.GetComponent<Rigidbody>().mass;
                currentCarryingWeight = cycleWeight + currentCarryingWeight;

                if (currentCarryingWeight <= StrengthManager.Instance.currentWorldInfo.maxWeightCarry)
                {
                    if (IsConnectRope())
                    {
                        Debug.Log("BIKE COLLISION");
                        GrapplingHook hook = collision.transform.GetChild(0).GetComponent<GrapplingHook>();

                        hook.ThrowHook();
                        attachedRopeCounter++;
                        isRopeAttached = true;
                        collision.transform.GetComponent<Rigidbody>().isKinematic = false;
                        collision.transform.GetComponent<Vehicle>().isAlreadyCollided = true;
                    }
                }
                else
                {
                    currentCarryingWeight -= cycleWeight;
                    Debug.LogWarning("WEIGHT EXCREEEEEDDD!! " + currentCarryingWeight);
                }
            }
        }
        if (collision.transform.CompareTag("TRUCK"))
        {
            if (!collision.transform.GetComponent<Vehicle>().isAlreadyCollided)
            {
                float cycleWeight = collision.transform.GetComponent<Rigidbody>().mass;
                currentCarryingWeight = cycleWeight + currentCarryingWeight;

                if (currentCarryingWeight <= StrengthManager.Instance.currentWorldInfo.maxWeightCarry)
                {
                    if (IsConnectRope())
                    {
                        Debug.Log("BIKE COLLISION");
                        GrapplingHook hook = collision.transform.GetChild(0).GetComponent<GrapplingHook>();
                        hook.ThrowHook();
                        isRopeAttached = true;
                        attachedRopeCounter++;
                        collision.transform.GetComponent<Rigidbody>().isKinematic = false;
                        collision.transform.GetComponent<Vehicle>().isAlreadyCollided = true;
                    }
                }
                else
                {
                    currentCarryingWeight -= cycleWeight;
                    Debug.LogWarning("WEIGHT EXCREEEEEDDD!! " + currentCarryingWeight);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.CompareTag("GYM"))
        {
            isInGym = true;
           
            UIManager.Instance.ActivateGymPanel();
            Gameplay.Instance.DeactivateInput();
            transform.position = gymPos.position;
            transform.rotation = Quaternion.Euler(new Vector3(0, 140, 0));
            camFollow.isUpgradeCamera = true;

        }
    }

    public void GymCloseButton()
    {
        isInGym = false;
        camFollow.isUpgradeCamera = false;
    }

    public void PlayIdle()
    {
        animator.SetTrigger(IDLE);
    }
    public void PlayRun()
    {
        animator.SetTrigger(RUN);
    }
    public void PlayDrag()
    {
        animator.SetTrigger(DRAG);
    }
    public IEnumerator PlayDumble()
    {
        barBell1.SetActive(true);
        barbell2.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        animator.SetTrigger(DUMBLE);
    }
    public IEnumerator PlaySitup()
    {
        dumbBell.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        animator.SetTrigger(SIT_UP);
    }
    public IEnumerator PlayTrademill()
    {
        tradMill.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        animator.SetTrigger(TRADE_MILL);
    }

    public void PlayGymMenu(GymMenu gymMenu)
    {
        isgymAnim = true;
        switch (gymMenu)
        {
            case GymMenu.DUMBLE:
               StartCoroutine(PlayDumble());
                break;
            case GymMenu.SITUP:
                StartCoroutine(PlaySitup());
                break;
            case GymMenu.TRADEMILL:
                StartCoroutine(PlayTrademill());
                break;
            default:
                break;
        }
    }

    public void ResetGym()
    {
        PlayIdle();
        isgymAnim = false;
        dumbBell.SetActive(false);
        barBell1.SetActive(false);
        barbell2.SetActive(false) ;
        tradMill.SetActive(false);
    }

    public bool IsConnectRope()
    {
        int maxRopeValue = StrengthManager.Instance.currentWorldInfo.maxRopeNumber;

        if(attachedRopeCounter != maxRopeValue)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void DecreaseRopeCounter()
    {
        attachedRopeCounter--;
        if (attachedRopeCounter < 0)
        {
            attachedRopeCounter = 0;
        }

        if(attachedRopeCounter == 0)
        {
            isRopeAttached = false;
        }
    }

    public void DecreaseWeight(float value)
    {
        currentCarryingWeight -= value;

        Debug.Log(currentCarryingWeight + " After decresasing");
        if(currentCarryingWeight <0)
        {
            currentCarryingWeight = 0;
        }
    }

}
