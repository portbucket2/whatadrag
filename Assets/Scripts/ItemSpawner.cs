using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public bool isOccupied;

    public ItemType itemType;
    public Vector3 offsetPos;

    public List<GameObject> itemList;


    private void Start()
    {
        SetYPos();
        //InstantiateItem();
    }

   

    public void InstantiateItem()
    {
        switch (itemType)
        {
            case ItemType.CYCLE:
                GameObject cycle = Instantiate(itemList[0]);
                cycle.transform.GetChild(0).GetComponent<Vehicle>().SetSpawner(this);
                cycle.transform.position = new Vector3(transform.position.x, offsetPos.y, transform.position.z);//0.95f
                cycle.transform.rotation = Quaternion.Euler(new Vector3(cycle.transform.rotation.x, Random.Range(65, 140), Random.Range(20, 55)));
                isOccupied = true;
                break;
            case ItemType.BIKE:
                GameObject bike = Instantiate(itemList[1]);
                bike.transform.GetChild(0).GetComponent<Vehicle>().SetSpawner(this);
                bike.transform.position = new Vector3(transform.position.x, offsetPos.y, transform.position.z); //0.75f
                bike.transform.rotation = Quaternion.Euler(new Vector3(bike.transform.rotation.x, Random.Range(65, 100), Random.Range(0, 10)));
                isOccupied = true;
                break;
            case ItemType.CAR:
                GameObject car = Instantiate(itemList[2]);
                car.transform.GetChild(0).GetComponent<Vehicle>().SetSpawner(this);
                car.transform.position = new Vector3(transform.position.x, offsetPos.y, transform.position.z); //0.75f
                isOccupied = true;
                break;
            case ItemType.TRUCK:
                GameObject truck = Instantiate(itemList[3]);
                truck.transform.GetChild(0).GetComponent<Vehicle>().SetSpawner(this);
                truck.transform.position = new Vector3(transform.position.x, offsetPos.y, transform.position.z); //1.2f
                isOccupied = true;
                break;
            default:
                break;
        }
    }

    public void SetYPos()
    {
        switch (itemType)
        {
            case ItemType.CYCLE:
                offsetPos.y = 1.5f;
                break;
            case ItemType.BIKE:
                offsetPos.y = 1.85f;
                break;
            case ItemType.CAR:
                offsetPos.y = 1.6f;
                break;
            case ItemType.TRUCK:
                offsetPos.y = 2.35f;
                break;
            default:
                break;
        }
    }
   
}
