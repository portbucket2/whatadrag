using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StrengthManager : MonoBehaviour
{
    public int currentStrength;
    private string CURRENT_STREGTH = "current_stregth";

    [Range(0, 1000)] public int maxStrenght;

    public BarUI strenghtBAR;

    public TextMeshProUGUI strenghtShowText;

    public WorldInfo currentWorldInfo;

    public List<WorldInfo> worldInfos;

    public int savedWorldId;
    private string WORLD_ID_PREF_KEY = "world_id";

    public GameObject skinnyChar;
    public GameObject strongChar;


    public static StrengthManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        savedWorldId = PlayerPrefs.GetInt(WORLD_ID_PREF_KEY, 0);
        if(savedWorldId == 0)
        {
            worldInfos[0].isWorldUnlocked = true;
            currentWorldInfo = worldInfos[0];
            strenghtShowText.text = currentWorldInfo.playerSkillShow;

            savedWorldId = currentWorldInfo.worldID;
            PlayerPrefs.SetInt(WORLD_ID_PREF_KEY, savedWorldId);
        }
        else
        {
            for (int j = 0; j < savedWorldId; j++)
            {
                worldInfos[j].isWorldUnlocked = true;
            }
            worldInfos[savedWorldId - 1].isWorldUnlocked = true;
            currentWorldInfo = worldInfos[savedWorldId - 1];
            strenghtShowText.text = currentWorldInfo.playerSkillShow;

            int i = savedWorldId - 1;

            if (i > 3)
            {
                skinnyChar.SetActive(false);
                strongChar.SetActive(true);
            }
        }

        currentStrength = PlayerPrefs.GetInt(CURRENT_STREGTH, 0);

        AnalyticsAssitantGM.ReportLevelStart(LevelManager.Instance.GetCurrentLevel());
    }

    public void UpdateCurrentStrength(int updateValue)
    {
        currentStrength += updateValue;

        PlayerPrefs.SetInt(CURRENT_STREGTH, currentStrength);


        float value = currentStrength  * 1f/ maxStrenght;
       
        Debug.Log(value);

        strenghtBAR.LoadValue(value, 1);

      StartCoroutine(CheckWorldUnlockedValue());
    }

    public IEnumerator CheckWorldUnlockedValue()
    {
        if (currentStrength >= 1000)
        {
            AnalyticsAssitantGM.ReportLevelCompleted(LevelManager.Instance.GetCurrentLevel());
            UIManager.Instance.NewWorldUnlock();
        }
        else
        {
            for (int i = 1; i < worldInfos.Count; i++)
            {
                if (currentStrength > worldInfos[i].worldUnlockedValue)
                {
                    if (!worldInfos[i].isWorldUnlocked)
                    {
                        AnalyticsAssitantGM.ReportLevelCompleted(LevelManager.Instance.GetCurrentLevel());

                        yield return new WaitForEndOfFrame();

                        LevelManager.Instance.IncreaseGameLevel();

                        AnalyticsAssitantGM.ReportLevelStart(LevelManager.Instance.GetCurrentLevel());

                        Debug.LogWarning(worldInfos[i].description + " UNLOCKED UNLOCKED!!");

                        worldInfos[i].isWorldUnlocked = true;

                        currentWorldInfo = worldInfos[i];
                        strenghtShowText.text = currentWorldInfo.playerSkillShow;

                        

                        savedWorldId = currentWorldInfo.worldID;
                        PlayerPrefs.SetInt(WORLD_ID_PREF_KEY, savedWorldId);

                        if(i > 3)
                        {
                            skinnyChar.SetActive(false);
                            strongChar.SetActive(true);
                        }
                    }
                }
            }
        }
    }


    public void ResetStreghtAndWorld()
    {
        PlayerPrefs.DeleteKey(CURRENT_STREGTH);
        PlayerPrefs.DeleteKey(WORLD_ID_PREF_KEY);
    }

    public void LoadBar()
    {
        float value = currentStrength * 1f / maxStrenght;
        Debug.Log(value);
        strenghtBAR.LoadValue(value, 1,true);
    }
}
