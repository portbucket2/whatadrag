using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GymObjective : MonoBehaviour
{
    public GymMenu gymMenu;
    public Button button;
    public BarUI barUI;

    public TextMeshProUGUI description;
    public TextMeshProUGUI coinText;

    public Objective objective;
    public Image itemIcon;

    public GameObject energyPanel;
    public TextMeshProUGUI energyValue;



    private void Start()
    {
        button.onClick.AddListener(delegate
        {
            Action OnFinished = OnFinishedCall;
            barUI.LoadValue(1, 1,false, true, OnFinished);

            PlayerController.Instance.PlayGymMenu(gymMenu);

            Currency.Transaction(CurrencyType.CASH, -objective.deductCoin);

            UIManager.Instance.DeactivateGymObjectList();
        });
    }

    private void OnFinishedCall()
    {
        Debug.LogWarning("FINISHED CALLING");

        StartCoroutine(WaitandReset());
    }

    IEnumerator WaitandReset()
    {
        energyPanel.SetActive(true);
        energyValue.text = objective.earnedPlayerStrength.ToString();
        yield return new WaitForSeconds(0.7f);
        StrengthManager.Instance.UpdateCurrentStrength(objective.earnedPlayerStrength);
        barUI.ResetBar();
        PlayerController.Instance.ResetGym();

        UIManager.Instance.ActivateGymObjectList();
    }

    public void SetInfo(Objective objective)
    {
        this.objective = objective;
        description.text = objective.objectiveDespriction;
        coinText.text = objective.deductCoin.ToString();

        ActivateButton();

        switch (gymMenu)
        {
            case GymMenu.DUMBLE:
                itemIcon.sprite = objective.itemIcon;
                break;
            case GymMenu.SITUP:
                itemIcon.sprite = objective.itemIcon;
                break;
            case GymMenu.TRADEMILL:
                itemIcon.sprite = objective.itemIcon;
                break;

            default:
                break;
        }
    }

    public void DeactivateEnergyPanel()
    {
        energyPanel.SetActive(false);
    }

    public void ActivateButton()
    {
        if (Currency.Balance(CurrencyType.CASH) < objective.deductCoin)
        {
            DeactivateButton();
        }
        else
        {
            button.interactable = true;
        }
    }
    public void DeactivateButton()
    {
        button.interactable = false;
    }
}
