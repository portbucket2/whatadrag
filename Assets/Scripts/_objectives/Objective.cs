using UnityEngine;

[CreateAssetMenu(fileName = "Objective", menuName = "WhatADrag/GymObjectives", order = 1)]
public class Objective : ScriptableObject
{
    public string objectiveName;
    public string objectiveDespriction;
    public float duration;
    public int earnedEnergy;
    public int earnedPlayerStrength;
    public int deductCoin;
    public Sprite itemIcon;
}
