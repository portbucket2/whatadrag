using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    public GameObject inputObject;
    public PlayerController playerController;

    public SpawnManager cycleSpawn;
    public SpawnManager bikeSpawn;
    public SpawnManager carSpawn;
    public SpawnManager truckSpawn;



    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }

    private IEnumerator StartGameRoutine()
    {
        yield return new WaitForEndOfFrame();
        inputObject.SetActive(true);
        playerController.ActivateDynamicJoystick();
    }

    public void DeactivateInput()
    {
        //inputObject.SetActive(false);
        playerController.DeactivateDynamicJoystick();
    }
    public void ActivateInput()
    {
        inputObject.SetActive(true);
        playerController.ActivateDynamicJoystick();
    }

    public void InstantiateItem(ItemType itemType)
    {
        switch (itemType)
        {
            case ItemType.CYCLE:
                cycleSpawn.InstantiateItem();
                break;
            case ItemType.BIKE:
                bikeSpawn.InstantiateItem();
                break;
            case ItemType.CAR:
                carSpawn.InstantiateItem();
                break;
            case ItemType.TRUCK:
                truckSpawn.InstantiateItem();
                break;

            default:
                break;
        }

        
    }
}
