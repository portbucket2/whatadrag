using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public List<ItemSpawner> spawnerList;

    public int spawnNumber;

    public static SpawnManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private IEnumerator Start()
    {
        int i = 0;
        do
        {
            int randVal = Random.Range(0, spawnerList.Count);
            if (!spawnerList[randVal].isOccupied)
            {
                yield return new WaitForEndOfFrame();
                spawnerList[randVal].InstantiateItem();
                i++;
            }
        } while (i < spawnNumber);
    }

    public void InstantiateItem()
    {
        int rand = Random.Range(0, spawnerList.Count);
        if (!spawnerList[rand].isOccupied)
        {
            Debug.LogWarning("INSTANTIATE CYCLE");
            spawnerList[rand].InstantiateItem();
        }
    }
}
