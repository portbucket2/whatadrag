public enum ItemType
{
    NONE,
    CYCLE,
    BIKE,
    CAR,
    TRUCK
}

[System.Serializable]
public class WorldInfo
{
    public string worldName;
    public string description;
    public ItemType unlockedItem;
    public int worldUnlockedValue;
    public int maxRopeNumber;
    public bool isWorldUnlocked;
    public string playerSkillShow;
    public int maxWeightCarry;
    public int worldID;
}

public enum GymMenu
{
    NONE,
    DUMBLE,
    SITUP,
    TRADEMILL
}