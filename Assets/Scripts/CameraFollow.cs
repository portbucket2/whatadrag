using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
	public Transform targetPlayer;

	[Range(0f,10f)] public float smoothing = 5f;

	public bool isUpgradeCamera;
	public Transform upgradeCamTrans;


	public Vector3 offset;


	// Use this for initialization
	void Start()
	{
		offset = transform.position - targetPlayer.position;
	}

	// Update is called once per frame
	void LateUpdate()
	{
        if (isUpgradeCamera)
        {
			transform.position = Vector3.Lerp(transform.position, upgradeCamTrans.position, 3f * Time.deltaTime);
			transform.rotation = Quaternion.Euler(new Vector3(50, 0, 0));
        }
        else
        {
			Vector3 targetCamPos = targetPlayer.position + offset;
			transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
			transform.rotation = Quaternion.Euler(new Vector3(60, 0, 0));
		}
	}
}
