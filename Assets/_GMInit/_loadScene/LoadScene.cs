using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        LoadNextScene();
    }

    public void LoadNextScene()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();

        SceneManager.LoadScene("WADGameplay");
    }
}
