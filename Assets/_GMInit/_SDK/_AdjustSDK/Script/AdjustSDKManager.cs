﻿using System;
using System.Collections.Generic;
using com.adjust.sdk;
using UnityEngine;

public class AdjustSDKManager : MonoBehaviour {

    #region Custom Variables

    [System.Serializable]
    public class AdjustEventTracker {

#if UNITY_EDITOR

        public bool showOnEditor = true;

#endif

        public string uniqueIdentifier = "";
        public string eventTokenForAndroid = "{EventToken = Android}";
        public string eventTokenForIOS = "{EventToken = iOS}";
    }

    #endregion

    #region Public Variables

    public static AdjustSDKManager Instance;

#if UNITY_EDITOR

    public bool updateAdjustConfiguretion;
    public bool showAdjustConfiguretionHelpBox;
    public bool showAdjustConfiguretion;
    public bool showAdvanceAdjustConfiguretion;

    public bool updateEnumListForPreConfigEventTracker;
    public bool showAdjustPreConfigEventTrackerHelpBox;
    public bool showAdjustPreConfigEventTracker;

    public bool updateEnumListForPreConfigRevenueEventTracker;
    public bool showAdjustPreConfigRevenueEventTrackerHelpBox;
    public bool showAdjustPreConfigRevenueEventTracker;

    public bool showDelayStartPanel;

#endif

    public string appTokenForAndroid;
    public string appTokenForIOS;

    //public bool isProductionBuild = false;
    public bool startManually = true;
    public AdjustLogLevel logLevel = AdjustLogLevel.Verbose;
    public AdjustEnvironment environment = AdjustEnvironment.Sandbox;

    //Parameter :   Adjust
    public bool eventBuffering;
    public bool sendInBackground;
    public bool launchDeferredDeeplink = true;
    public float startDelay = 0.05f;
    public List<AdjustEventTracker> listOfAdjustEventTracker;

    #endregion

    #region Private Variables

    private string appToken;

    #endregion

    #region Mono Behaviour

    private void Awake () {

#if UNITY_IOS
        appToken = appTokenForIOS;
#elif UNITY_ANDROID
        appToken = appTokenForAndroid;
#endif

        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        } else {

            Destroy (gameObject);
        }

        if (!startManually)
        {
            Initialization();
        }
    }

    #endregion

    #region Configuretion   :   Adjust

    private bool IsValidPreConfigEvent (int t_PreConfigEventIndex) {

        if (t_PreConfigEventIndex >= 0 && t_PreConfigEventIndex < listOfAdjustEventTracker.Count)
            return true;

        return false;
    }

    private int GetPreConfigEventIndexFromUniqueIdentifier (string t_UniqueIdentifierForEvent) {

        int t_NumberOfEvent = listOfAdjustEventTracker.Count;
        for (int i = 0; i < t_NumberOfEvent; i++) {

            if (t_UniqueIdentifierForEvent == listOfAdjustEventTracker[i].uniqueIdentifier)
                return i;
        }

        return -1;
    }

    #endregion

    #region Public Callback

    public void Initialization () {

        StartAdjust ();
    }

    #endregion

    #region Public Callback :   Adjust

    public void StartAdjust () {

        AdjustConfig adjustConfig = new AdjustConfig (appToken, environment, logLevel == AdjustLogLevel.Suppress);
        adjustConfig.setLogLevel (logLevel);
        adjustConfig.setSendInBackground (sendInBackground);
        adjustConfig.setEventBufferingEnabled (eventBuffering);
        adjustConfig.setLaunchDeferredDeeplink (launchDeferredDeeplink);

        adjustConfig.setDelayStart (startDelay);

        Adjust.start (adjustConfig);
    }

    public void TrackAdjustEvent (string t_EventToken) {

        AdjustEvent t_NewAdjustEvent = new AdjustEvent (t_EventToken);
        Adjust.trackEvent (t_NewAdjustEvent);
    }

    public void TrackRevenue (string t_EventToken, double t_Amount, string t_Currency = "USA") {

        AdjustEvent t_NewAdjustEvent = new AdjustEvent (t_EventToken);
        t_NewAdjustEvent.setRevenue (t_Amount, t_Currency);
        Adjust.trackEvent (t_NewAdjustEvent);
    }

    public void TrackRevenueWithDeDuplication (string t_EventToken, double t_Amount, string t_TransactionID, string t_Currency = "USA") {

        AdjustEvent t_NewAdjustEvent = new AdjustEvent (t_EventToken);
        t_NewAdjustEvent.setRevenue (t_Amount, t_Currency);
        t_NewAdjustEvent.setTransactionId (t_TransactionID);
        Adjust.trackEvent (t_NewAdjustEvent);
    }

    public void InvokePreConfigEvent (AdjustEventTrack t_EventUniqueIdentifier) {

        InvokePreConfigEvent (GetPreConfigEventIndexFromUniqueIdentifier (t_EventUniqueIdentifier.ToString ()));
    }

    public void InvokePreConfigEvent (int t_EventIndex) {

        if (IsValidPreConfigEvent (t_EventIndex)) {

            string t_EventToken = "";

#if UNITY_IOS
            t_EventToken = listOfAdjustEventTracker[t_EventIndex].eventTokenForIOS;
#elif UNITY_ANDROID
            t_EventToken = listOfAdjustEventTracker[t_EventIndex].eventTokenForAndroid;
#endif

            TrackAdjustEvent (t_EventToken);
        }
    }

    public void StartAdjustAfterMaxSDKInitialized()
    {
        if (!startManually)
        {
            Initialization();
        }
    }

    #endregion

    private static bool IsEditor()
    {
#if UNITY_EDITOR
        return true;
#else
            return false;
#endif
    }

    private void OnApplicationPause(bool pause)
    {
        if(IsEditor()) { return; }

#if UNITY_IOS
        // Nothing
#elif UNITY_ANDROID
        if (pause)
        {
            AdjustAndroid.OnPause();
        }
        else
        {
            AdjustAndroid.OnResume();
        }
#else
    Debug.Log("NO ANDROID OR IOS");
#endif
    }
}