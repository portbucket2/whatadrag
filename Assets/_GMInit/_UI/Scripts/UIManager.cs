﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public UnityEvent OnGameStartEvent;

    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;

    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;

    [Header("Upper panel")] public Animator upperPanelAnimator;
    public TextMeshProUGUI currentLevelText;


    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;
    public ParticleSystem levelCompleteParticle2;

    public GameObject tutObject;
    public Button playButton;

    private int tutShow;
    private string TUT_SHOW = "tut_show";



    [Header("Level Failed")] public GameObject levelFailed;
    public Button tryAgainButton;

    [Header("GYM UI")]
    public GameObject gymPanel;
    public Button gymCloseButton;
    public ParticleSystem energyParticle;


    public List<GymObjective> gymObjectiveList;
    public List<Objective> objectivesList;

    [Header("New World")]
    public GameObject newWorldPanel;
    public Button newWorldButton;


    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    private void Start()
    {
        currentLevelText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        ButtonInteraction();

    }

    
    public void LoadLevelNumber()
    {
        currentLevelText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
    }


    private void ButtonInteraction()
    {
        tapButton.onClick.AddListener(delegate
        {
            startPanelAnimator.SetTrigger(EXIT);
            // upperPanelAnimator.SetTrigger(ENTRY);
            StartCoroutine(Wait());
        });
    }

    IEnumerator Wait()
    {
        tutShow = PlayerPrefs.GetInt(TUT_SHOW, 0);
        if (tutShow == 0)
        {
            PlayerPrefs.SetInt(TUT_SHOW, 1);
            yield return new WaitForSeconds(0.5f);
            ActivateTut();
        }
        else
        {
            yield return new WaitForEndOfFrame();
            OnGameStartEvent.Invoke();
        }
    }

    public void ShowLevelComplete()
    {
        levelCompleteAnimator.SetTrigger(ENTRY);

        levelCompleteParticle.Play();
        levelCompleteParticle2.Play();

        collectButton.onClick.AddListener(delegate
        {
            
        });
    }
    public void ActivateTut()
    {
        tutObject.SetActive(true);
        playButton.onClick.RemoveAllListeners();
        playButton.onClick.AddListener(delegate
        {
            tutObject.SetActive(false);
            OnGameStartEvent.Invoke();
        });
    }
    public void DeactivateTut()
    {
        tutObject.SetActive(false);
    }

    public void LevelFailed()
    {
        levelFailed.SetActive(true);
        tryAgainButton.onClick.AddListener(delegate
        {
            

        });
    }

    public void ActivateGymPanel()
    {
        gymPanel.SetActive(true);

        for (int i = 0; i < gymObjectiveList.Count; i++)
        {
            gymObjectiveList[i].SetInfo(objectivesList[i]);
        }

        gymCloseButton.onClick.RemoveAllListeners();
        gymCloseButton.onClick.AddListener(delegate
        {
            gymPanel.SetActive(false);
            Gameplay.Instance.ActivateInput();
            PlayerController.Instance.GymCloseButton();
        });

        StrengthManager.Instance.LoadBar();

    }

    
    public void DeactivateGymPanel()
    {
        gymPanel.SetActive(false);
    }

    public void PlayEnergyParticle()
    {
        energyParticle.Play();
    }
    public void DeactivateGymObjectList()
    {
        gymCloseButton.interactable = false;
        for (int i = 0; i < gymObjectiveList.Count; i++)
        {
            gymObjectiveList[i].DeactivateButton();
        }
    }
    public void ActivateGymObjectList()
    {
        gymCloseButton.interactable = true;
        for (int i = 0; i < gymObjectiveList.Count; i++)
        {
            gymObjectiveList[i].ActivateButton();
        }
    }

    public void NewWorldUnlock()
    {
        Gameplay.Instance.inputObject.SetActive(false);
        newWorldPanel.SetActive(true);
        newWorldButton.onClick.RemoveAllListeners();
        newWorldButton.onClick.AddListener(delegate
        {
            LevelManager.Instance.IncreaseGameLevel();

            StrengthManager.Instance.ResetStreghtAndWorld();
            int curBalance = Currency.Balance(CurrencyType.CASH);
            Currency.Transaction(CurrencyType.CASH, -curBalance);

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        });
    }
}
