﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

/**
 * Sample component that shows how to use Obi Rope to create a grappling hook for a 2.5D game.
 * 95% of the code is the grappling hook logic (user input, scene raycasting, launching, attaching the hook, etc) and parameter setup,
 * to show how to use Obi completely at runtime. This might not be practical for real-world scenarios,
 * but illustrates how to do it.
 *
 * Note that the choice of using actual rope simulation for grapple dynamics is debatable. Usually
 * a simple spring works better both in terms of performance and controllability. 
 *
 * If complex interaction is required with the scene, a purely geometry-based approach (ala Worms ninja rope) can
 * be the right choice under certain circumstances.
 */
public class GrapplingHook : MonoBehaviour
{

    public ObiSolver solver;
    public ObiCollider character;
    public float hookExtendRetractSpeed = 2;
    public Material material;
    public ObiRopeSection section;


    public Transform playerTrans;

    public float ropeLength;


    private ObiRope rope;
    private ObiRopeBlueprint blueprint;
    private ObiRopeExtrudedRenderer ropeRenderer;

    private ObiRopeCursor cursor;

    private RaycastHit hookAttachment;

    void Awake()
    {

        // Create both the rope and the solver:	
        rope = gameObject.AddComponent<ObiRope>();
        ropeRenderer = gameObject.AddComponent<ObiRopeExtrudedRenderer>();
        ropeRenderer.section = section;
        ropeRenderer.uvScale = new Vector2(1, 5);
        ropeRenderer.normalizeV = false;
        ropeRenderer.uvAnchor = 1;
        rope.GetComponent<MeshRenderer>().material = material;

        // Setup a blueprint for the rope:
        blueprint = ScriptableObject.CreateInstance<ObiRopeBlueprint>();
        blueprint.resolution = 0.5f;

        // Tweak rope parameters:
        rope.maxBending = 0.02f;

        // Add a cursor to be able to change rope length:
        cursor = rope.gameObject.AddComponent<ObiRopeCursor>();
        cursor.cursorMu = 0;
        cursor.direction = true;

        rope.maxBending = 0.5f;
        rope.plasticYield = 0.1f;
    }
    private void Start()
    {
        playerTrans = PlayerController.Instance.transform;
    }

    private void OnDestroy()
    {
        DestroyImmediate(blueprint);
    }

    /**
	 * Raycast against the scene to see if we can attach the hook to something.
	 */
    private void LaunchHook()
    {
        // Get a ray from the character to the mouse:
        Ray ray = new Ray(transform.position, (playerTrans.position-transform.position));

        Debug.DrawRay(transform.position, (playerTrans.position - transform.position),Color.red);

        // Raycast to see what we hit:
        if (Physics.Raycast(ray, out hookAttachment))
        {
            // We actually hit something, so attach the hook!
            StartCoroutine(AttachHook());
        }
    }

    private IEnumerator AttachHook()
    {
        Debug.Log("Routine");
        yield return 0;
        Vector3 localHit = rope.transform.InverseTransformPoint(playerTrans.position);

        int filter = ObiUtils.MakeFilter(ObiUtils.CollideWithEverything,0);

        // Procedurally generate the rope path (a simple straight line):
        blueprint.path.Clear();
        blueprint.path.AddControlPoint(Vector3.zero, -localHit.normalized, localHit.normalized, Vector3.up, 0.1f, 0.1f, 1, filter, Color.white, "Hook start");
        blueprint.path.AddControlPoint(localHit, -localHit.normalized, localHit.normalized, Vector3.up, 0.1f, 0.1f, 1, filter, Color.white, "Hook end");
        blueprint.path.FlushEvents();

        // Generate the particle representation of the rope (wait until it has finished):
        yield return blueprint.Generate();
        
        // Set the blueprint (this adds particles/constraints to the solver and starts simulating them).
        rope.ropeBlueprint = blueprint;
        rope.GetComponent<MeshRenderer>().enabled = true;

        // Pin both ends of the rope (this enables two-way interaction between character and rope):
        var pinConstraints = rope.GetConstraintsByType(Oni.ConstraintType.Pin) as ObiConstraints<ObiPinConstraintsBatch>;
        pinConstraints.Clear();
        var batch = new ObiPinConstraintsBatch();
        batch.AddConstraint(rope.solverIndices[0], character, transform.localPosition, Quaternion.identity, 0, 0, float.PositiveInfinity);
        batch.AddConstraint(rope.solverIndices[blueprint.activeParticleCount - 1], playerTrans.GetComponent<ObiColliderBase>(),
                                                          playerTrans.InverseTransformPoint(playerTrans.position), Quaternion.identity, 0, 0, float.PositiveInfinity);
        batch.activeConstraintCount = 2;
        pinConstraints.AddBatch(batch);

        rope.SetConstraintsDirty(Oni.ConstraintType.Pin);

        yield return StartCoroutine(RopeIncreaseRoutine());

        Debug.LogError(rope.restLength);
    }

    IEnumerator RopeIncreaseRoutine()
    {
        float t_Progression = 0f;
        float t_Duration = 1f;
        float t_EndTIme = Time.time + t_Duration;
        float t_CurrentFillValue = rope.restLength;
        float t_DestValue = ropeLength;


        WaitForEndOfFrame t_CycleDelay = new WaitForEndOfFrame();

        character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;

        //Platform Movement
        while (true)
        {
            t_Progression = 1 - ((t_EndTIme - Time.time) / t_Duration);
            cursor.ChangeLength (Mathf.Lerp(t_CurrentFillValue, t_DestValue, t_Progression));

            if (t_Progression >= 1f)
            {
                character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                character.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
                break;
            }

            yield return t_CycleDelay;
        }

        StopCoroutine(RopeIncreaseRoutine());
    }



    private void DetachHook()
    {
        // Set the rope blueprint to null (automatically removes the previous blueprint from the solver, if any).
        rope.ropeBlueprint = null;
        rope.GetComponent<MeshRenderer>().enabled = false;
    }


    public void ThrowHook()
    {
        if (!rope.isLoaded)
        {
            Debug.Log("Attach Called");
            LaunchHook();
        }
    }

    void Update()
    {

        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (!rope.isLoaded)
        //    {
        //        Debug.Log("Attach Called");
        //        LaunchHook();
        //    }
        //   // else
        //       // DetachHook();
        //}

        if (rope.isLoaded)
        {
            if (Input.GetKey(KeyCode.W))
            {
                cursor.ChangeLength(rope.restLength - hookExtendRetractSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                cursor.ChangeLength(rope.restLength + hookExtendRetractSpeed * Time.deltaTime);
            }
        }
    }
}
